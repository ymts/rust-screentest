
//时间库
use chrono::prelude::*;

//随机数生成库
use rand::{Rng};


use std::io::{self, Write};
use std::cmp::PartialEq;
use std::sync::Mutex;

lazy_static! {
    static ref P: Mutex<()> = Mutex::new(());
}


//window api
#[cfg(windows)]
extern crate winapi;

pub unsafe fn serialize_row<T: Sized>(src: &T) -> &[u8] {
    ::std::slice::from_raw_parts((src as *const T) as *const u8, ::std::mem::size_of::<T>())
}

// 截屏返回bmp图片数据
fn screen() -> Vec<u8> {
    use std::mem;
    use winapi::shared::minwindef::LPVOID;
    use winapi::shared::windef::{HDC, HGDIOBJ, RECT};
    use winapi::um::{
        wingdi::{
            CreateCompatibleBitmap, CreateCompatibleDC, CreateDIBSection, DeleteDC, GetDIBits,
            GetObjectW, SelectObject, StretchBlt, BITMAPFILEHEADER, BITMAPINFO, BITMAPINFOHEADER,
            DIB_RGB_COLORS, RGBQUAD, SRCCOPY,
        },
        winnt::HANDLE,
        winuser::{GetDesktopWindow, GetWindowDC, GetWindowRect},
    };
    let mut data: Vec<u8> = vec![];
    unsafe {
        let hwnd = GetDesktopWindow();
        let dc = GetWindowDC(hwnd);
        let cdc = CreateCompatibleDC(0 as HDC);
        let mut rect = RECT {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
        };
        GetWindowRect(hwnd, &mut rect);
        let (w, h) = (rect.right, rect.bottom);
        let bm = CreateCompatibleBitmap(dc, w, h);
        SelectObject(cdc, bm as HGDIOBJ);
        StretchBlt(cdc, 0, 0, w, h, dc, 0, 0, w, h, SRCCOPY);
        let buf = vec![0u8; (w * h * 4) as usize];
        GetObjectW(bm as HANDLE, 84, buf.as_ptr() as LPVOID);
        let mut bi = BITMAPINFO {
            bmiHeader: BITMAPINFOHEADER {
                biBitCount: 32,
                biWidth: w,
                biHeight: h,
                biPlanes: 1,
                biCompression: 0,
                biSizeImage: 0,

                biClrImportant: 0,
                biClrUsed: 0,
                biSize: 0,
                biXPelsPerMeter: 0,
                biYPelsPerMeter: 0,
            },
            bmiColors: [RGBQUAD {
                rgbBlue: 0,
                rgbGreen: 0,
                rgbRed: 0,
                rgbReserved: 0,
            }; 1],
        };
        bi.bmiHeader.biSize = mem::size_of_val(&bi.bmiHeader) as u32;
        CreateDIBSection(
            cdc,
            &bi,
            DIB_RGB_COLORS,
            buf.as_ptr() as *mut *mut winapi::ctypes::c_void,
            0 as HANDLE,
            0,
        );

        GetDIBits(
            cdc,
            bm,
            0,
            h as u32,
            buf.as_ptr() as LPVOID,
            &mut bi,
            DIB_RGB_COLORS,
        );

        let bif = BITMAPFILEHEADER {
            bfType: ('B' as u16) | (('M' as u16) << 8),
            bfOffBits: 54,
            bfReserved1: 0,
            bfReserved2: 0,
            bfSize: (w * h * 4 + 54) as u32,
        };

        for v in serialize_row(&bif) {
            data.push(*v);
        }
        let bii = BITMAPINFOHEADER {
            biBitCount: 32,
            biSize: 40,
            biWidth: w,
            biHeight: h,
            biPlanes: 1,
            biCompression: 0,
            biSizeImage: (w * h * 4) as u32,
            biClrImportant: 0,
            biClrUsed: 0,
            biXPelsPerMeter: 0,
            biYPelsPerMeter: 0,
        };

        for v in serialize_row(&bii) {
            data.push(*v);
        }

        for v in buf {
            data.push(v);
        }

        DeleteDC(dc);
        DeleteDC(cdc);
    }
    data
}

fn saveImg() {
    use std::fs::File;
    use std::io::Write;

    let data = screen();


    let now = Local::now();
    let ndt = now.naive_local();//本地时间
    let res = match Local.from_local_datetime(&ndt).single() {
        Some(v) => v,
        None => panic!("错误"),
    };

    //时间格式化
    let formatted_time = res.format("%Y%m%d%H%M%S");

    //随机数
    let mut rng = rand::thread_rng();
    let n: u16 = rng.gen_range(100..999);

    let file_name=formatted_time.to_string()+"--"+ &*n.to_string() +".bmp";
    let mut file = File::create(file_name).expect("create failed");
    file.write_all(&data[..]).expect("write failed");
}



fn main() {
    let mut running = true;

    let mut event_loop = EventLoop::new();

    event_loop.add_raw_key_handler(
        KeyCode(Key::from_char('q').unwrap()),
        Box::new(move |state, target: &mut EventLoop| {
            if state == KeyState::Pressed {
                target.shutdown();
            }
        }),
    );

    let mut stdout = io::stdout();
    let mut stderr = io::stderr();

    let (mut tx, mut rx) = channel::<()>(1);
    let (mut inner_tx, mut inner_rx) = channel::<()>(1);

    thread::spawn(move || {
        while let Some(()) = rx.recv() {
            if P.lock().unwrap().peek() {
                inner_tx.send(()).unwrap();
            } else {
                inner_tx.send(()).unwrap();
                stdout.write_all(b"Hello World\n").unwrap();
            }
        }
    });

    event_loop.run(move |event| {
        use Event;

        match event {
            Event::KeyboardInput(state, key, mods) => {
                if state == KeyState::Pressed {
                    if key == KeyCode(Key::from_char('a').unwrap()) {
                        P.lock().unwrap().set();
                    } else if key == KeyCode(Key::from_char('s').unwrap()) {
                        P.lock().unwrap().reset();
                    } else if key == KeyCode(Key::from_char('c').unwrap()) {
                        tx.send(()).unwrap();
                    } else {
                        stderr.write_all(format!("Other key: {:?}\n").as_bytes()).unwrap();
                    }
                }
            }
            Event::Termination(reason) => {
                inner_rx.recv().unwrap();
                running = false;
            }
            _ => {}
        }

        if running { Ok(()) } else { Err(io::Error::new(io::ErrorKind::Other, "Termination")) }
    });
}